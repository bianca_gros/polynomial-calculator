package Controller;
import View.*;
import Model.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    View v = new View("Control");

    public Controller(){
        // butonul pentru adunare
        v.getSumButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!v.getFirstPoly().getText().isEmpty() && !v.getSecondPoly().getText().isEmpty()){
                    Polynomial p1 = new Polynomial(v.getFirstPoly().getText());
                    Polynomial p2 = new Polynomial(v.getSecondPoly().getText());
                    Calculus calc = new Calculus(p1, p2);
                    Polynomial rez = calc.addition();
                    v.getResultPoly().setText(rez.toString());
                }
                else{
                    Error2Operands window = new Error2Operands();
                }
            }
        });

        // butonul pentru scadere
        v.getSubstrButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!v.getFirstPoly().getText().isEmpty() && !v.getSecondPoly().getText().isEmpty()){
                    Polynomial p1 = new Polynomial(v.getFirstPoly().getText());
                    Polynomial p2 = new Polynomial(v.getSecondPoly().getText());
                    Calculus calc = new Calculus(p1, p2);
                    Polynomial rez = calc.substraction();
                    v.getResultPoly().setText(rez.toString());
                }
                else{
                    Error2Operands window = new Error2Operands();
                }
            }
        });

        // buton pentru inmultire
        v.getMulButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!v.getFirstPoly().getText().isEmpty() && !v.getSecondPoly().getText().isEmpty()){
                    Polynomial p1 = new Polynomial(v.getFirstPoly().getText());
                    Polynomial p2 = new Polynomial(v.getSecondPoly().getText());
                    Calculus calc = new Calculus(p1, p2);
                    Polynomial rez = calc.multiplication();
                    v.getResultPoly().setText(rez.toString());
                }
                else{
                    Error2Operands window = new Error2Operands();
                }
            }
        });

        // buton pentru impartire
        v.getDivButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!v.getFirstPoly().getText().isEmpty() && !v.getSecondPoly().getText().isEmpty()){
                    Polynomial p1 = new Polynomial(v.getFirstPoly().getText());
                    Polynomial p2 = new Polynomial(v.getSecondPoly().getText());
                    Calculus calc = new Calculus(p1, p2);
                    Polynomial[] rez = calc.division();
                    v.getResultPoly().setText(rez[0].toString());
                    v.getRemainderPoly().setText(rez[1].toString());
                }
                else{
                    Error2Operands window = new Error2Operands();
                }
            }
        });

        // buton pentru derivare
        v.getDerivButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!v.getFirstPoly().getText().isEmpty()){
                    Polynomial p1 = new Polynomial(v.getFirstPoly().getText());
                    Polynomial p2 = new Polynomial(v.getSecondPoly().getText());
                    Calculus calc = new Calculus(p1, p2);
                    Polynomial rez = calc.derivative(p1);
                    v.getResultPoly().setText(rez.toString());
                }
                else{
                    Error1Operand window = new Error1Operand();
                }
            }
        });

        // buton pentru integrare
        v.getIntegrButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!v.getFirstPoly().getText().isEmpty()){
                    Polynomial p1 = new Polynomial(v.getFirstPoly().getText());
                    Polynomial p2 = new Polynomial(v.getSecondPoly().getText());
                    Calculus calc = new Calculus(p1, p2);
                    Polynomial rez = calc.integral(p1);
                    v.getResultPoly().setText(rez.toString());
                }
                else{
                    Error1Operand window = new Error1Operand();
                }
            }
        });

        // buton pentru stergere
        v.getClear().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                v.getFirstPoly().setText("");
                v.getSecondPoly().setText("");
                v.getResultPoly().setText("");
                v.getRemainderPoly().setText("");
            }
        });
    }

    public static void main(String[] args) {
        Controller control = new Controller();
    }
}
