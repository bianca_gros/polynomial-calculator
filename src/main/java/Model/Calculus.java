package Model;
import java.util.Comparator;

public class Calculus {
    private Polynomial p1, p2;

    public Calculus(Polynomial p1, Polynomial p2){
        this.p1 = p1;
        this.p2 = p2;
    }

    public Calculus() { }

    public void setPol(Polynomial p1, Polynomial p2){
        this.p1 = p1;
        this.p2 = p2;
    }

    public Polynomial addition(){
        Polynomial p3 = new Polynomial();
        for(Monomial m : p1.getPoly()){ // parcurgem monoamele din p1
            int pow1 = m.getPow(); // pentru fiecare monom luam puterea
            double coef1 = m.getCoef(); // si coeficientul
            int i_pow = p2.getPowIndex(pow1);
            if(i_pow == -1){ // daca puterea nu se gaseste si intr-un monom din p2
                p3.getPoly().add(new Monomial(coef1, pow1)); // atunci fa un monom nou corespunzator acestei puteri
            }
            else{ // daca puterea se gaseste in p2
                int pow3 = p2.getPoly().get(i_pow).getPow();
                double coef3 = p2.getPoly().get(i_pow).getCoef();
                p3.getPoly().add(new Monomial(coef1 + coef3, pow3)); // fa un monom ce are ca si coeficient suma celor doi coeficienti ai monoamelor cu putera cautata
                p2.getPoly().remove(i_pow); // sterge din p2 ce a fost "numarat"
            }
        }
        for(Monomial n : p2.getPoly()){ // restul monoamelor din p2
            int pow2 = n.getPow();
            double coef2 = n.getCoef();
            p3.getPoly().add(new Monomial(coef2, pow2)); // adauga-le la celelalte monoame din rezultat
        }
        p3.getPoly().sort(new Comparator<Monomial>() { // sorteaza monoamele in ordinea puterilor
            @Override
            public int compare(Monomial x, Monomial y)
            {
                Integer i = y.getPow();
                return  i.compareTo(x.getPow());
            }});
        return p3;
    }

    public Polynomial substraction(){// acelasi procedeu ca la adunare, doar ca in cazul puterilor egale coeficientii se scad iar termenii ramasi se "aduna" cu "-" in polinomul rezultat p3
        Polynomial p3 = new Polynomial();
        for(Monomial m : p1.getPoly()){
            int pow1 = m.getPow();
            double coef1 = m.getCoef();
            int i_pow = p2.getPowIndex(pow1);
            if(i_pow == -1){
                p3.getPoly().add(new Monomial(coef1, pow1));
            }
            else{
                int pow3 = p2.getPoly().get(i_pow).getPow();
                double coef3 = p2.getPoly().get(i_pow).getCoef();
                p3.getPoly().add(new Monomial(coef1 - coef3, pow3));
                p2.getPoly().remove(i_pow);
            }
        }
        for(Monomial n : p2.getPoly()){
            int pow2 = n.getPow();
            double coef2 = n.getCoef();
            p3.getPoly().add(new Monomial( - coef2, pow2));
        }
        p3.getPoly().sort(new Comparator<Monomial>() {
            @Override
            public int compare(Monomial x, Monomial y)
            {
                Integer i = new Integer(y.getPow());
                return  i.compareTo(x.getPow());
            }});
        return p3;
    }

    public Polynomial multiplication(){
        Polynomial p3 = new Polynomial();
        for(Monomial m : p1.getPoly()){
            for(Monomial n : p2.getPoly()){
                Monomial z = new Monomial(m.getCoef() * n.getCoef(), m.getPow() + n.getPow());
                p3.getPoly().add(z);
            }
        }
        p3.getPoly().sort(new Comparator<Monomial>() { // sorteaza monoamele in ordinea puterilor
            @Override
            public int compare(Monomial x, Monomial y)
            {
                Integer i = new Integer(y.getPow());
                return  i.compareTo(x.getPow());
            }});
        p3.reduce(p3);
        return p3;
    }

    public Polynomial[] division(){
        Polynomial q = new Polynomial();
        Polynomial r = new Polynomial(p1);
        double c = p2.getPoly().get(0).getCoef();
        int p = p2.getMaxPower();
        while(r.getMaxPower() >= p){
            double lcr = 0;
            for(Monomial i:r.getPoly())
                if(i.getPow() == r.getMaxPower()) {
                    lcr = i.getCoef();
                }
            double s = lcr / c;
            Polynomial aux = new Polynomial(new Monomial(s, r.getMaxPower() -  p));
            Calculus f = new Calculus(q,aux);
            q = f.addition();
            Polynomial x = new Polynomial(new Monomial(s * (-1), r.getMaxPower() - p));
            Calculus w = new Calculus(p2, x);
            Polynomial k = w.multiplication();
            Calculus ww = new Calculus(r,k);
            r = ww.addition();
            if(!r.getPoly().isEmpty()) {
                for(Monomial j: r.getPoly()){
                    if(j.getCoef() == 0){
                        r.getPoly().remove(j);
                        break;
                    }
                }
            }
       }
        return new Polynomial[]{q,r};
    }

    public Polynomial derivative(Polynomial p2){
        Polynomial p3 = new Polynomial();
        for(Monomial n : p2.getPoly()){
            n.setCoef(n.getCoef() * n.getPow());
            n.setPow(n.getPow() - 1);
            p3.getPoly().add(n);
        }
        return p3;
    }

    public Polynomial integral (Polynomial p2){
        Polynomial p3 = new Polynomial();
        for(Monomial n : p2.getPoly()){

            if(n.getPow() == 0){
                n.setPow(n.getPow() + 1);
            }
            else{
                n.setPow(n.getPow() + 1);
                n.setCoef(n.getCoef() / n.getPow());
                }

            p3.getPoly().add(n);
        }
        return p3;
    }

}
