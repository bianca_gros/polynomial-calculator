package Model;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class Polynomial {
    private ArrayList<Monomial> poly = new ArrayList<Monomial>();

    public Polynomial(Polynomial p) {
        this.poly = p.getPoly();
    }

    public Polynomial(Monomial m){
        this.poly.add(m);
    }

    public Polynomial(){

    }

    public Polynomial(String pol) { // construieste ArrayList ul de monoame al polinomului
        double coef = 0;
        int pow = 0;
        String polNew = pol.replaceAll("-", "+-"); // pune +- in loc de -
        String[] monomials = polNew.split("\\+"); // face split in monoame (face un tablou de string uri in care pune monoame)
        for (int i = 0; i < monomials.length; i++) { // itereaza prin monoame
            if (!monomials[i].equals("")) {
                String[] mon = monomials[i].split("x\\^"); // desparte in monoame
                for (int j = 0; j < mon.length; j++) {
                    if (mon[j].contains("x")) {
                        String[] except = mon[j].split("x");
                        if (except.length == 0) {
                            coef = 1;
                            pow = 1;
                        } else {
                            if (except.length == 1) {
                                if (except[0].equals("-")) {
                                    coef = -1;
                                    pow = 1;
                                } else {
                                    coef = Integer.parseInt(except[0]);
                                    pow = 1;
                                }
                            }
                        }
                    } else {
                        if (mon.length == 1) {
                            coef = Integer.parseInt(mon[0]);
                            pow = 0;
                        } else {
                            try {
                                coef = Integer.parseInt(mon[0]);
                                pow = Integer.parseInt(mon[1]);
                            } catch (NumberFormatException e) {
                                if (mon[0].equals("-")) {
                                    coef = -1;
                                } else {
                                    coef = 1;
                                }
                            }
                            pow = Integer.parseInt(mon[1]);
                        }
                    }
                }
                if (coef != 0) { // daca coeficientul nu este 0
                    poly.add(new Monomial(coef, pow)); // adauga monomul in ArrayList ul de monoame al polinomului
                }
            }
        }
    }

    public String toString() {
        String print = "";
        int i = 0;
        for (Monomial m : poly) {
            i++;
            if (m.getCoef() != 0) {
                print = print + m.getCoef() + "x^" + m.getPow();
            }
            if (i != poly.size()) {
                print += "+";
            }
        }
        return print;
    }

    public int getMinPower() {
        int p;
        String polNew = this.toString();
        polNew = polNew.replaceAll("-", "+-");
        String[] monomials = polNew.split("\\+");
        String[] mon = monomials[monomials.length - 1].split("(x\\^)");
        p = Integer.parseInt(mon[1]);
        return p;
    }

    public int getMaxPower() {
        /*
        String polNew = this.toString();
        polNew = polNew.replaceAll("-", "+-");
        String[] monomials = polNew.split("\\+");
        String[] mon = monomials[0].split("(x\\^)");
        p = Integer.parseInt(mon[1]);
         */
        int max = 0;

        for(Monomial i:this.poly){
            if(i.getPow() >= max)
                max = i.getPow();
        }
        return max;
    }

    public int getPowIndex(int x) {
        int power = -1;
        for (int i = 0; i < this.getPoly().size(); i++) {
            if (this.getPoly().get(i).getPow() == x) {
                power = i;
            }
        }
        return power;
    }

    public ArrayList<Monomial> getPoly() {
        return poly;
    }

    public void setPoly(ArrayList<Monomial> pol) {
        this.poly = pol;
    }

    public Polynomial reduce(Polynomial p3) {
        for (int i = 0; i < p3.getPoly().size(); i++) { // reducerea termenilor asemenea
            for (int j = i + 1; j < p3.getPoly().size(); j++) {
                if (p3.getPoly().get(j).getPow() == p3.getPoly().get(i).getPow()) {
                    p3.getPoly().get(i).setCoef(p3.getPoly().get(i).getCoef() + p3.getPoly().get(j).getCoef());
                    p3.getPoly().remove(j);
                    j--;
                }
            }
        }
        return p3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Polynomial)) return false;
        Polynomial that = (Polynomial) o;
        return Objects.equals(getPoly(), that.getPoly());
    }

    public int compareTo(Object o) {
        Polynomial p = (Polynomial) o;
        if (this.getMaxPower() == p.getMaxPower()) {
            return 0;
        } else {
            if (this.getMaxPower() < p.getMaxPower()) {
                return -1;
            } else
                return 1;
        }
    }
}
