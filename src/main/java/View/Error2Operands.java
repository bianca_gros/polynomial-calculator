package View;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Error2Operands {
    private JFrame frame = new JFrame("Error entering data");
    private JPanel pane = new JPanel();
    private JButton but = new JButton("Got it!");
    private JLabel label = new JLabel("     Enter 2 valid operands to perform the selected operation      ");

    public Error2Operands(){
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,100);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        pane.add(label);
        pane.add(but);
        but.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        frame.setContentPane(pane);
    }
}
