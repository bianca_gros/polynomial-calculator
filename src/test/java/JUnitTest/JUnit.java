package JUnitTest;
import Model.*;
import static org.junit.Assert.*;
import org.junit.*;

public class JUnit {

    private static Calculus c;
    private static int nbOfExecutedTests = 0;
    private static int nbOfSuccessTests = 0;

    public JUnit()
    {
        System.out.println();
        System.out.println("Constructor before each test!");
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("Only once, before executing the set of tests for this class!");
        c = new Calculus();
    }
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("Only once, after executing the set of tests for this class!");
        System.out.println();
        System.out.println( nbOfExecutedTests + " tests executed, " + nbOfSuccessTests + " tests successfully executed!");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("New test starts now!");
        nbOfExecutedTests++;
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Current test is over!");
    }

    @Test
    public void testAddition() {
        Polynomial p1 = new Polynomial("2x^3+x^2-1");
        Polynomial p2 = new Polynomial("3x^2+x");
        c = new Calculus(p1, p2);
        Polynomial p3 = new Polynomial();
        p3 = c.addition();
        assertNotNull(p3);
        assertEquals(p3.toString(),"2.0x^3+4.0x^2+1.0x^1+-1.0x^0");
        nbOfSuccessTests++;
    }

    @Test
    public void testSubstraction() {
        Polynomial p1 = new Polynomial("2x^3+x^2-1");
        Polynomial p2 = new Polynomial("3x^2+x");
        c = new Calculus(p1, p2);
        Polynomial p3 = new Polynomial();
        p3 = c.substraction();
        assertNotNull(p3);
        assertEquals(p3.toString(),"2.0x^3+-2.0x^2+-1.0x^1+-1.0x^0");
        nbOfSuccessTests++;
    }

    @Test
    public void testMultiplication() {
        Polynomial p1 = new Polynomial("2x^3+x^2-1");
        Polynomial p2 = new Polynomial("3x^2");
        c = new Calculus(p1, p2);
        Polynomial p3 = new Polynomial();
        p3 = c.multiplication();
        assertNotNull(p3);
        assertEquals(p3.toString(),"6.0x^5+3.0x^4+-3.0x^2");
        nbOfSuccessTests++;
    }

    @Test
    public void testDivision() {
        Polynomial p1 = new Polynomial("x^4+10x^2+1");
        Polynomial p2 = new Polynomial("2x^2");
        c = new Calculus(p1, p2);
        Polynomial[] p = new Polynomial[2];
        p = c.division();
        Polynomial q = p[0];
        Polynomial r = p[1];
        assertNotNull(q);
        assertEquals(q.toString(),"0.5x^2+5.0x^0");
        assertEquals(r.toString(),"1.0x^0");
        nbOfSuccessTests++;
    }

    @Test
    public void testDerivative() {
        Polynomial p1 = new Polynomial("2x^3+x^2");
        Polynomial p2 = new Polynomial("3x^2");
        c = new Calculus(p1, p2);
        Polynomial p3 = new Polynomial();
        p3 = c.derivative(p1);
        assertNotNull(p3);
        assertEquals(p3.toString(),"6.0x^2+2.0x^1");
        nbOfSuccessTests++;
    }

    @Test
    public void testIntegral() {
        Polynomial p1 = new Polynomial("2x^3+x^2");
        Polynomial p2 = new Polynomial("3x^2");
        c = new Calculus(p1, p2);
        Polynomial p3 = new Polynomial();
        p3 = c.integral(p2);
        assertNotNull(p3);
        assertEquals(p3.toString(),"1.0x^3");
        nbOfSuccessTests++;
    }

}
